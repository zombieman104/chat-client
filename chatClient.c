#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

//Worked with Sam, Maya, and Ben Riley

int main(){
	struct sockaddr_in serv; //Primary socket variable
	int fd;	//Socket file descriptor that identifies the socket

	char message[100] = "";	//array that stores messages that are sent by the server
	char username[20];
	char buffer[80];
	char button;
	
	fd = socket(AF_INET, SOCK_STREAM, 0);	//creates a new socket that returns the identifier of the socket to the file descriptor

	serv.sin_family = AF_INET;		//chooses IPv4
	serv.sin_port = htons(49153);	//defines the port at which the server will listen for connections

	inet_pton(AF_INET, "10.115.20.250", &serv.sin_addr); 	//binds the westmont pilot ip to the local host

	connect(fd, (struct sockaddr *)&serv, sizeof(serv)); //connects client to server

	printf("Press Enter to Start\n");

	gets(username);
	printf("Hi %s, I am connecting you to the server \n" ,username);
	
	if(send(fd, username, strlen(username),0)<0) { 		//sends username to server and checks for error
		printf("Failed to send\n");
	}


	if(read(fd, buffer, 80)<0){ 		//Client reads data from the server
		printf("Failed to buffer\n");
	}		

	printf("%s\n", buffer);
	

	printf("%s\n", "Insert Username Please");
	while(1){

		//Message capability
		printf("Enter Message: ");
		fgets(message, 100, stdin);
		send(fd, message, strlen(message), 0);
		
		read(fd, buffer, 100);		//reads data from server

		printf("%s\n", buffer);		//Prints data from the server when pressing enter		

	}
}